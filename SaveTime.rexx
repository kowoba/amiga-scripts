/*
  $VER: SaveTime 1.5 (29.01.2023) Kolbj�rn Barmen <amiga@kolla.no>
*/

IF OPEN( TS, 'S:Time-Startup', WRITE )
  THEN
    CALL WRITELN( TS, 'Date' TRANSLATE( DATE( E ), '-', '/') TIME() )
  ELSE
    DO
      ECHO "Failed writing to S:Time-Startup!"
      EXIT 30
    END
