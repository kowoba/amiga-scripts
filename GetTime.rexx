/*
  $VER: GetTime.rexx 1.2 (31.08.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

GTS='PIPE:GT'

IF ~OPEN(GT, GTS, WRITE)
  THEN
    EXIT 30

DO
  CALL WRITELN(GT, 'Run >NIL: Ping ONEREPLY ${NTP} QUIET +')
  CALL WRITELN(GT, 'Wait 4 +')
  CALL WRITELN(GT, 'SNTP ${NTP}')
  CALL CLOSE(GT)
END

ADDRESS COMMAND Execute GTS
