/*
  $VER: AmiChores 47.8 (31.08.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

IF EXISTS("REXX:AmiChores/Startup.rexx")
  THEN
    "AmiChores/Startup"

DO FOREVER

  _DATE = DATE(S)

  YEAR        = SUBSTR( _DATE, 1, 4)
  YEARSHORT   = SUBSTR( _DATE, 3, 2)
  MONTH       = SUBSTR( _DATE, 5, 2)
  MONTHDAY    = SUBSTR( _DATE, 7, 2)
  MONTHNAME   = DATE(M)
  MONTHSHORT  = SUBSTR( MONTHNAME, 1, 3)

  DAY    = DATE(W)
  DAYMIN = TIME(M)
  HOUR   = SUBSTR(100 + DAYMIN % 60, 2, 2)
  MINUTE = SUBSTR(100 + DAYMIN // 60, 2, 2)

  IF EXISTS("REXX:AmiChores/Minutely.rexx")
    THEN
      "AmiChores/Minutely"

  IF MINUTE//5 = 0 & EXISTS("REXX:AmiChores/5-Minutes.rexx")
    THEN
      "AmiChores/5-Minutes"

  IF MINUTE//10 = 0 & EXISTS("REXX:AmiChores/10-Minutes.rexx")
    THEN
      "AmiChores/10-Minutes"

  IF MINUTE//15 = 0 & EXISTS("REXX:AmiChores/15-Minutes.rexx")
    THEN
      "AmiChores/15-Minutes"

  IF MINUTE//20 = 0 & EXISTS("REXX:AmiChores/20-Minutes.rexx")
    THEN
      "AmiChores/20-Minutes"

  IF MINUTE//30 = 0 & EXISTS("REXX:AmiChores/30-Minutes.rexx")
    THEN
      "AmiChores/30-Minutes"

  IF MINUTE//60 = 0 & EXISTS("REXX:AmiChores/Hourly.rexx")
    THEN
      "AmiChores/Hourly"

  IF EXISTS("REXX:AmiChores/" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || DAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || DAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTH || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTH || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHNAME || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHNAME || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || YEAR || MONTH || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || YEAR || MONTH || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || YEAR || "-" || MONTH || "-" || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || YEAR || "-" || MONTH || "-" || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || YEARSHORT || MONTH || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || YEARSHORT || MONTH || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || YEARSHORT || "-" || MONTH || "-" || MONTHDAY || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || YEARSHORT || "-" || MONTH || "-" || MONTHDAY || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHNAME || "-" || YEAR || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHNAME || "-" || YEAR || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHNAME || "-" || YEARSHORT || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHNAME || "-" || YEARSHORT || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "-" || YEAR || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "-" || YEAR || "_" || HOUR || MINUTE

  IF EXISTS("REXX:AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "-" || YEARSHORT || "_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/" || MONTHDAY || "-" || MONTHSHORT || "-" || YEARSHORT || "_" || HOUR || MINUTE

  IF DAY ~= "Saturday" & DAY ~= "Sunday" & EXISTS("REXX:AmiChores/Weekday_" || HOUR || MINUTE || ".rexx")
    THEN
      "AmiChores/Weekday_" || HOUR || MINUTE

  ADDRESS COMMAND 'Wait 59'
END
