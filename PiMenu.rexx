/*
  PiMenu.rexx 1.1 (23.09.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

ADDRESS WORKBENCH

IF ~EXISTS('ENV:PIMENU')
  THEN
    PIMENU = OFF

IF OPEN(A, 'ENV:PIMENU', R)
  THEN
    PIMENU = READLN(A)

IF PIMENU = "GO"
  THEN
    DO
      RX 'PiMenu'
      ADDRESS COMMAND 'SetEnv PIMENU ON'
    END

If PIMENU = OFF
  THEN
    RX 'PiMenu REMOVE'
