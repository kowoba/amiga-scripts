/*
  $VER: ListMenus.rexx 1.1 (02.04.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

PARSE ARG PATTERN

ADDRESS WORKBENCH

GETATTR MENUCOMMANDS STEM menu_list

IF LENGTH(PATTERN) == 0
  THEN 
    DO i = 0 TO (menu_list.count - 1)
      SAY menu_list.i.NAME
    END
  ELSE
    DO i = 0 TO (menu_list.count - 1)
      IF SUBSTR(menu_list.i.NAME, 1, LENGTH(PATTERN)) == PATTERN
        THEN
          SAY menu_list.i.NAME
    END
