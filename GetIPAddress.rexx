/*
  GetIPAdress.rexx 1.4 (22.09.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

ADDRESS COMMAND

TF = "PIPE:GIA"
'ShowNetStatus INTERFACES >' || TF

IF ~OPEN(H, TF, R)
  THEN
    EXIT

CALL READLN(H)
DO UNTIL EOF(H)
  S = READLN(H)

  PARSE VAR S NAME MTU TYPE ADDR RX TX DROP OVRR UNKN LINK .

  IF NAME = 'Name'
    THEN
      ITERATE

  IF ADDR = '-'
    THEN
      ADDR = 'N/A'

  IF LINK = "Up" | LINK = "Down"
    THEN
      DO
        'SetEnv INTERFACE' NAME
        'SetEnv IPADDRESS' ADDR
        EXIT
      END
END

IF NAME = 'Name'
  THEN
    DO
      'UnSetEnv INTERFACE'
      'UnSetEnv IPADDRESS'
    END
