/*
  ShowPorts.rexx 1.1 (02-02-2023) Kolbj�rn Barmen <amiga@kolla.no>
*/

ports = show( ports,,";") || ";"

do until ports = ""
  i = pos(";", ports)
  say left( ports, i - 1 )
  ports = substr( ports, i + 1 )
end
