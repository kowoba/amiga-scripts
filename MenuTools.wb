/*
  $VER: MenuTools.wb 47.19 (29.12.2024) Kolbj�rn Barmen <amiga@kolla.no>
*/

PARSE ARG ACTION

MENUTAG     = "tools_"
MENUWAIT    = "Vennligst vent"
MENURELOAD  = "Scan p� nytt"
MENUEDIT    = "Rediger"
MENUCOPY    = "Kopier"
MENUPASTE   = "Lim inn"
MENUWINDOW  = "Vindauge"
MENUICON    = "Ikon"
MENUHELP    = "Hjelp"
MENUREBOOT  = "Omstart"

IF EXISTS("C:Ed")               THEN EDITOR = "C:Ed"
IF EXISTS("SYS:Tools/TextEdit") THEN EDITOR = "SYS:Tools/TextEdit"
IF EXISTS("SYS:Tools/EditPad")  THEN EDITOR = "SYS:Tools/EditPad"
IF EXISTS("ENV:EDITOR")         THEN EDITOR = "${EDITOR}"
IF EXISTS("S:Editor")           THEN EDITOR = "S:Editor"

IF ACTION == "RELOAD"
  THEN
    DO
      MENU ADD NAME '"waitbar_' || MENUTAG || '"' TITLE '"~"' CMD ''
      MENU ADD NAME '"wait_'    || MENUTAG || '"' TITLE '"' || MENUWAIT || '..."' CMD ''
    END

RX RemoveMenus MENUTAG

IF ACTION == "REMOVE"
  THEN
    EXIT

IF ACTION == "RELOAD"
  THEN
    MENU REMOVE NAME '"waitbar_' || MENUTAG || '"'
  ELSE
    MENU ADD NAME '"wait_' || MENUTAG || '"' TITLE '"' || MENUWAIT || '..."' CMD ''

MENU ADD NAME '"waitbar_' || MENUTAG || '"' TITLE '"~"' CMD ''

IF EXISTS("SYS:WBStartup/WBDock.info") | EXISTS("SYS:WBStartup/NewMeter.info")
  THEN
    MENU ADD NAME MENUTAG || 'RsWB' TITLE '"ResetWB+"'                                     CMD "ResetWB"
MENU ADD NAME MENUTAG || 'ClRm' TITLE '"T�m RAM"'                                          CMD "Execute S:CleanRAM"
MENU ADD NAME MENUTAG || 'Copy' TITLE '"' || MENUEDIT   || '\' || MENUCOPY || '"'          CMD "Copy"
MENU ADD NAME MENUTAG || 'Past' TITLE '"' || MENUEDIT   || '\' || MENUPASTE || '"'         CMD "Paste"
MENU ADD NAME MENUTAG || 'Sep2' TITLE '"~"'                                                CMD ""
MENU ADD NAME MENUTAG || 'Shll' TITLE '"Shell"'                                            CMD "WBLoad SYS:System/Shell"
MENU ADD NAME MENUTAG || 'OpDr' TITLE '"' || MENUWINDOW || '\Opne vindauge..."'            CMD "OpenSelDrawer"
MENU ADD NAME MENUTAG || 'Sep5' TITLE '"' || MENUWINDOW || '\~"'                           CMD ""
MENU ADD NAME MENUTAG || 'ClAD' TITLE '"' || MENUWINDOW || '\Lukk alle vindauge"'          CMD "CloseAllDrawers"
MENU ADD NAME MENUTAG || 'ClID' TITLE '"' || MENUWINDOW || '\Lukk inaktive vindauge"'      CMD "CloseExceptActiveWin"
MENU ADD NAME MENUTAG || 'ClPD' TITLE '"' || MENUWINDOW || '\Lukk foreldervindauge"'       CMD "CloseTopWin"
MENU ADD NAME MENUTAG || 'ClCD' TITLE '"' || MENUWINDOW || '\Lukk barnevindauge"'          CMD "CloseSubWin"
MENU ADD NAME MENUTAG || 'Sep6' TITLE '"' || MENUWINDOW || '\~"'                           CMD ""
MENU ADD NAME MENUTAG || 'RmDr' TITLE '"' || MENUWINDOW || '\Hugs opne vindauge"'          CMD "SaveOpenDrawers"
MENU ADD NAME MENUTAG || 'RgDr' TITLE '"' || MENUWINDOW || '\Gl�ym opne vindauge"'         CMD "ForgetOpenDrawers"
MENU ADD NAME MENUTAG || 'IcDe' TITLE '"' || MENUICON   || '\Lag standard"'  SHORTCUT "%"  CMD "CreateDefaults"
MENU ADD NAME MENUTAG || 'IcSe' TITLE '"' || MENUICON   || '\Vel ikon..."'   SHORTCUT "/"  CMD "SelPattIcons"
MENU ADD NAME MENUTAG || 'IcSo' TITLE '"' || MENUICON   || '\Ordne"'         SHORTCUT "."  CMD "IconSorter ORDER=TOOL PROJECT DRAWER SPACEX=25 SPACEY=5"
MENU ADD NAME MENUTAG || 'IcTo' TITLE '"' || MENUICON   || '\Byt valde"'     SHORTCUT "T"  CMD "ToggleIcon"
MENU ADD NAME MENUTAG || 'Sep3' TITLE '"' || MENUICON   || '\~"'                           CMD ""
MENU ADD NAME MENUTAG || 'IREM' TITLE '"' || MENUICON   || '\Remap 1.x"'                   CMD "ProcessIcons REMAP"
MENU ADD NAME MENUTAG || 'IMCI' TITLE '"' || MENUICON   || '\MWB to CI"'                   CMD "ProcessIcons MWB2CI"
MENU ADD NAME MENUTAG || 'INCI' TITLE '"' || MENUICON   || '\NI to CI"'                    CMD "ProcessIcons NI2CI"
MENU ADD NAME MENUTAG || 'IICI' TITLE '"' || MENUICON   || '\II to CI"'                    CMD "ProcessIcons II2CI"
MENU ADD NAME MENUTAG || 'IMKC' TITLE '"' || MENUICON   || '\Make CI"'                     CMD "ProcessIcons MAKECI KNI OPT"
MENU ADD NAME MENUTAG || 'IOPT' TITLE '"' || MENUICON   || '\Optimize"'                    CMD "ProcessIcons OPT"
MENU ADD NAME MENUTAG || 'ISTT' TITLE '"' || MENUICON   || '\No border"'                   CMD "ProcessIcons ST1 0 ST2 0"
MENU ADD NAME MENUTAG || 'Sep4' TITLE '"' || MENUICON   || '\~"'                           CMD ""
MENU ADD NAME MENUTAG || 'IcUp' TITLE '"' || MENUICON   || '\Sjekk lib..."'                CMD "Execute GIT:Scripts/IconLib-Update"

CALL LocalMenus()
CALL DynaMenus(MENUTAG, "SYS:System",    "System")
CALL DynaMenus(MENUTAG, "SYS:Prefs",     "Prefs")
CALL DynaMenus(MENUTAG, "SYS:Tools",     "Tools")
CALL DynaMenus(MENUTAG, "SYS:Utilities", "Utils")

MENU ADD NAME MENUTAG || 'MTEd' TITLE '"MenuTool\' || MENUEDIT   || '..."' CMD "Run" EDITOR "REXX:MenuTools.wb"
MENU ADD NAME MENUTAG || 'MTRl' TITLE '"MenuTool\' || MENURELOAD || '"'    CMD "MenuTools"
MENU ADD NAME MENUTAG || 'Sep7' TITLE '"~"'                                CMD ""
MENU ADD NAME MENUTAG || 'Dpus' TITLE '"DOpus4"'                           CMD "WBLoad Program:DOpus4/DOpus4"
MENU ADD NAME MENUTAG || 'Ced4' TITLE '"Editor"'                           CMD "Run" EDITOR "S:Ukjent"
MENU ADD NAME MENUTAG || 'Exch' TITLE '"Exchange"'                         CMD "WBLoad SYS:Tools/Commodities/Exchange"
MENU ADD NAME MENUTAG || 'Sep8' TITLE '"~"'                                CMD ""
MENU ADD NAME MENUTAG || 'Help' TITLE '"' || MENUHELP || '"'               CMD "Run Help"
MENU ADD NAME MENUTAG || 'Sep9' TITLE '"~"'                                CMD ""
MENU ADD NAME MENUTAG || 'Boot' TITLE '"' || MENUREBOOT || '..."'          CMD "Execute S:App-Startup"

MENU REMOVE NAME '"wait_'    || MENUTAG || '"'
MENU REMOVE NAME '"waitbar_' || MENUTAG || '"'

EXIT

DynaMenus: PROCEDURE

  MENUTAG     = ARG(1)
  MENUPATH    = ARG(2)
  MENUTITLE   = ARG(3)
  MENUCOMMAND = "WBLoad"

  DYNAMENUS = 'PIPE:DM'

  ADDRESS COMMAND 'List TO' DYNAMENUS MENUPATH 'SORT NAME FILES PAT ~(#?.info) LFORMAT "%n"'

  PCOUNT=0

  IF ~OPEN(DM, DYNAMENUS, R)
    THEN
      RETURN

  DO UNTIL EOF(DM)
    P = READLN(DM)

    IF ~EXISTS(MENUPATH || "/" || P || ".info")
      THEN
        ITERATE

    PROGNAME = ToolType(MENUPATH || "/" || P 'PROGNAME')

    IF PROGNAME = "IGNORE"
      THEN
        ITERATE

    IF PROGNAME = ""
      THEN
        PROGNAME = P

    PCOUNT = PCOUNT + 1
    IF PCOUNT = 30
      THEN
        MENUTITLE = MENUTITLE || "+"

    MENU ADD NAME MENUTAG || MENUTITLE || "_" || P TITLE '"' || MENUTITLE || "\" || PROGNAME || '"' CMD MENUCOMMAND MENUPATH || "/" || P

  END

  CALL CLOSE(DM)
  RETURN

LocalMenus: PROCEDURE
  IF EXISTS("REXX:MenuLocal.wb")
    THEN
      CALL MenuLocal()
  RETURN
