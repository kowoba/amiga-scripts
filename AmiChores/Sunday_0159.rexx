/*
  AmiChores/Sunday-0159

  Extremely rudimentary DST handler
*/

IF DATE(D) > 83 & DATE(D) < 305 
  THEN
    ADDRESS COMMAND "SetENV SAVE TZONE CET-1CEST"
