/*
  AmiChores/Sunday-0259

  Extremely rudimentary DST handler
*/

IF DATE(D) < 83 | DATE(D) > 297
  THEN
    ADDRESS COMMAND "SetENV SAVE TZONE CET-0CET"
