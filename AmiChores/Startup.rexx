/*
  AmiChores/Startup
*/

ADDRESS COMMAND

DO seconds = 1 TO 20
  'Resident >PIPE:RES'
  'Search >NIL: PIPE:RES SNTP'
  IF RC = 0
    THEN
      DO
        Call GetTime
        LEAVE seconds
      END
  'Wait'
END

'Wait FILE ENV:PIMENU'
'RX PiMenu'
