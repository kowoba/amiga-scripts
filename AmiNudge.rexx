/*
  $VER: AmiNudge.rexx 1.5 (03.02.2025) Kolbj�rn Barmen <amiga@kolla.no>
*/

NUDGE_PATH  = 'T:'

REQ_TITLE   = "AmiNudge"
REQ_GADGETS = "OK"
REQ_IMAGE   = "Information"
REQ_COMMAND = "Run >NIL: RAChoice"

ADDRESS COMMAND 'List TO PIPE:AN' NUDGE_PATH 'PAT "AmiNudge.~(#?.info)" LFORMAT "%N"'

IF RIGHT(NUDGE_PATH, 1) ~= ':' & RIGHT(NUDGE_PATH, 1) ~= '/'
  THEN
    NUDGE_PATH = NUDGE_PATH || '/'

IF ~OPEN(AN, 'PIPE:AN', READ)
  THEN
    EXIT

DO UNTIL EOF(AN)
  NUDGE = READLN(AN)
  NUDGE_FILE = NUDGE_PATH || NUDGE

  IF NUDGE == ""
    THEN
      BREAK

  SOURCE = SUBSTR(NUDGE, POS('.', NUDGE) + 1)
  SOURCE = LEFT( SOURCE, POS('.', SOURCE) - 1)

  IF SOURCE == "rpi"
    THEN
      DO
        REQ_TITLE = "${RPI_HARDWARE}"
        REQ_IMAGE = "GIT:Scripts/RPi.info"
      END

  IF EXISTS('ENVARC:Sys/def_' || SOURCE || '.info')
    THEN
      REQ_IMAGE = 'ENVARC:Sys/def_' || SOURCE || '.info'

  IF EXISTS(NUDGE_FILE || '.info')
    THEN
      REQ_IMAGE = NUDGE_FILE || '.info'

  ADDRESS COMMAND
    REQ_COMMAND TITLE '"' || REQ_TITLE || '"' FILE '"' || NUDGE_FILE || '"' GADGETS '"' || REQ_GADGETS || '"' IMAGE '"' || REQ_IMAGE || '"'
    'Rename' NUDGE_FILE 'Done_' || NUDGE_FILE
    'DelayDelete Done_' || NUDGE_FILE 'QUIET'

  IF EXISTS(NUDGE_FILE || '.info')
    THEN
      'DelayDelete' NUDGE_FILE || '.info QUIET'

END

EXIT
