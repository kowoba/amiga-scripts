# Various Amiga scripts

* Aminet - An Amiga guide driven frontend to Aminet
* RPi - Wrapper around rsh, run remote commands
* RPi.info - RPi Icon for the abuse, used by RAChoice
* RPiGet - Wrapper around rsh, to retrieve files over rsh
* CURLGet - Wrapper around rsh, to use remote curl
* GitSync - Rsyncs down remote git repo to a GIT: assign
* GitPostSync - Do some cleanup after sync is done
* App-Startup - Reboot into a program, put in s-s somewhere
* App-Startup.info - Icon for the abuse, used by RAChoice
* IsFile - Checks whether argument is file, dir, assign..
* DelayDelete - Forever keeps trying to delete a file/drawer
* AmiChores.rexx - Do some chores at specific times, sort of cron
* AmiChores/Startup.rexx - A chore that runs on startup
* AmiChores/Minutely.rexx - A chore that runs every minute
* AmiChores/Sunday-0159.rexx - A rudimentary DST handler
* AmiChores/Sunday-0259.rexx - A rudimentary DST handler
* MenuTools.wb - Adds a bunch to the Tools menu
* MenuPrograms.wb - Adds Program:app/app programs to a Program menu
* PiMenu.wb - Adds a RPi menu with commands to run on remote RPi
* WBMenu - WBStartup wrapper for the above Workbench menus
* WBMenu.info - Just an icon for WBStartup
* MenuFy.wb - Adds a program to Workbench menus
* ListMenus.rexx - List Workbench REXX based menus ("MenuTools")
* RemoveMenus.wb - Removes Workbench REXX based menus ("MenuTools")
* ToolType.wb - Reads tooltypes from .info of specified file/dir
* WBLoad.wb - Workbench REXX Wrappr for WBLoad
* Execute.wb - Workbench REXX Wrappr for Execute
* Run.wb - Workbench REXX Wrappr for Run
* Help - Reimplemented SYS:System/Help of OS 3.2, simpler
* SPat - Updated for shell v47, no more silly assigns
* DPat - Updated for shell v47, no more silly assigns
* PCD - Updated for shell v47, no more silly assigns
* GetIPAddress.rexx - Get the local IP address for Roadshow
* ShowPorts.rexx - List out available AREXX ports
* CleanRAM - Clean out RAM: and flush resources
* LhArchiver - Simple Wrapper around LhA

Updated: 2024-04-29 03:35
